# Link with description

## Introduction

Provides a general field "Link with description" with same functionality as the
core Link field but with additional "description" field.


## Requirements

The core Link module.


## Installation

Install as you would normally install a contributed Drupal module.    
See: https://www.drupal.org/node/895232 for further information.


## Configuration

* Create a new field "Link with description" for an entity
* Set up the field formatter at entity display mode


## Maintainers

Current maintainers:
 * Timo Kirkkala (kirkkala) - https://www.drupal.org/u/kirkkala
