<?php

namespace Drupal\link_description\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'link_description' formatter.
 *
 * @FieldFormatter(
 *   id = "link_description",
 *   label = @Translation("Link with description"),
 *   field_types = {
 *     "link_description"
 *   }
 * )
 */
class LinkDescriptionFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    foreach ($items as $delta => $item) {
      // The parent class has already created the link with optional options.
      $link = $elements[$delta];
      // Change output to the template file with description.
      $elements[$delta] = [
        '#theme' => 'link_with_description',
        '#link' => $link,
      ];
      if ($item->description) {
        // The description value has no text format assigned to it, so the user
        // input should equal the output, including newlines.
        $elements[$delta]['#description'] = [
          '#type' => 'inline_template',
          '#template' => '{{ value|nl2br }}',
          '#context' => ['value' => $item->description],
        ];
      }
    }
    return $elements;
  }

}
