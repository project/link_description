<?php

namespace Drupal\link_description\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkSeparateFormatter;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "link_separate_description",
 *   label = @Translation("Title and link URL with description"),
 *   field_types = {
 *     "link_description"
 *   }
 * )
 */
class LinkSeparateDescriptionFormatter extends LinkSeparateFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    foreach ($items as $delta => $item) {
      // Alter formatter by displaying the additional description via template.
      $elements[$delta]['#theme'] = 'link_with_description_separate_text_url';
      if ($item->description) {
        // The description value has no text format assigned to it, so the user
        // input should equal the output, including newlines.
        $elements[$delta]['#description'] = [
          '#type' => 'inline_template',
          '#template' => '{{ value|nl2br }}',
          '#context' => ['value' => $item->description],
        ];
      }
    }
    return $elements;
  }

}
