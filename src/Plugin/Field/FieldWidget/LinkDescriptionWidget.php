<?php

namespace Drupal\link_description\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Plugin implementation of the 'link_description' widget.
 *
 * Extends the core link widget by adding a description field.
 *
 * @FieldWidget(
 *   id = "link_description",
 *   label = @Translation("Link description"),
 *   field_types = {
 *     "link_description"
 *   }
 * )
 */
class LinkDescriptionWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['description'] = [
      '#type' => 'textarea',
      '#rows' => 3,
      '#title' => $this->t('Long description'),
      '#default_value' => $items[$delta]->description ?? NULL,
    ];

    return $element;
  }

}
