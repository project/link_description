<?php

namespace Drupal\link_description\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Plugin implementation of the 'link_description' field type.
 *
 * Extends the core link field by adding a link description field.
 *
 * @FieldType(
 *   id = "link_description",
 *   label = @Translation("Link with description"),
 *   description = @Translation("Similar to core link field, stores the URL string, optional link text and description and optional blob of attributes."),
 *   default_widget = "link_description",
 *   default_formatter = "link_description"
 * )
 */
class LinkDescriptionItem extends LinkItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    // @todo Use "text" as the type definition.
    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('Link description'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['description'] = [
      'description' => 'The link description.',
      'type' => 'text',
      'size' => 'big',
    ];

    return $schema;
  }

}
