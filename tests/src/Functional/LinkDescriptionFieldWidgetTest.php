<?php

namespace Drupal\Tests\link_description\Functional;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests link description field widget.
 *
 * @group link_description
 */
class LinkDescriptionFieldWidgetTest extends LinkDescriptionFieldTestBase {

  /**
   * Tests 'description' field exists on the 'link_description' widget.
   */
  public function testLinkDescriptionWidget() {
    $entity_form_builder = \Drupal::service('entity.form_builder');
    $form = $entity_form_builder->getForm(EntityTest::create());
    $this->assertEquals('textarea', $form[$this->fieldName]['widget'][0]['description']['#type']);
    $this->assertEquals('Long description', $form[$this->fieldName]['widget'][0]['description']['#title']);
    $this->assertEmpty($form[$this->fieldName]['widget'][0]['description']['#default_value']);

    // Assert default value of the link description field.
    $entity_test = EntityTest::create([
      'name' => $this->randomMachineName(),
      $this->fieldName => [
        'uri' => 'https://www.example.com',
        'title' => 'Example site',
        'description' => 'Link description &',
      ],
    ]);
    $entity_test->save();
    $form = $entity_form_builder->getForm($entity_test);
    $this->assertEquals('Link description &', $form[$this->fieldName]['widget'][0]['description']['#default_value']);
  }

}
