<?php

namespace Drupal\Tests\link_description\Functional;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\entity_test\Entity\EntityTest;

/**
 * Tests link description field formatters.
 *
 * @group link_description
 */
class LinkDescriptionFieldFormattersTest extends LinkDescriptionFieldTestBase {

  /**
   * Tests the 'link_description' formatter.
   *
   * Rework of the test from Drupal\Tests\link\Functional\LinkFieldTest.
   */
  public function testLinkDescriptionFormatter() {
    $display_options = [
      'type' => 'link_description',
      'label' => 'hidden',
    ];
    $this->setViewDisplayOptions($display_options);

    // Create an entity with three link field values:
    // The first field item uses a URL only.
    // The second field item uses URL, link text.
    // The third field item uses a fragment-only URL with text and description.
    $this->drupalGet('entity_test/add');

    $url1 = 'http://www.example.com/content/articles/archive?author=John&year=2012#com';
    $url2 = 'http://www.example.org/content/articles/archive?author=John&year=2012#org';
    $url3 = '#net';
    $title1 = $url1;
    // Intentionally contains an ampersand that needs sanitization on output.
    $title2 = 'A very long & strange example title that could break the nice layout of the site';
    $title3 = 'Fragment only';
    $edit = [
      "{$this->fieldName}[0][uri]" => $url1,
      "{$this->fieldName}[1][uri]" => $url2,
      "{$this->fieldName}[1][title]" => $title2,
      "{$this->fieldName}[2][uri]" => $url3,
      "{$this->fieldName}[2][title]" => $title3,
      "{$this->fieldName}[2][description]" => "Link 1 & &amp;\nLine 2",
    ];
    // Assert label is shown.
    $this->assertSession()->pageTextContains('Read more about this entity');
    $this->submitForm($edit, 'Save');
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains('entity_test ' . $id . ' has been created.');

    $options = [
      'trim_length' => [NULL, 6],
      'rel' => [NULL, 'nofollow'],
      'target' => [NULL, '_blank'],
    ];
    foreach ($options as $setting => $values) {
      foreach ($values as $new_value) {
        // Update the field formatter settings.
        $display_options['settings'] = [$setting => $new_value];
        $this->setViewDisplayOptions($display_options);

        $output = $this->renderTestEntity($id);
        switch ($setting) {
          case 'trim_length':
            $url = $url1;
            $title = isset($new_value) ? Unicode::truncate($title1, $new_value, FALSE, TRUE) : $title1;
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url) . '">' . Html::escape($title) . '</a>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);

            $url = $url2;
            $title = isset($new_value) ? Unicode::truncate($title2, $new_value, FALSE, TRUE) : $title2;
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url) . '">' . Html::escape($title) . '</a>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);

            $url = $url3;
            $title = isset($new_value) ? Unicode::truncate($title3, $new_value, FALSE, TRUE) : $title3;
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url) . '">' . Html::escape($title) . '</a>';
            $expected .= "<p class=\"link-description\">Link 1 &amp; &amp;amp;<br />\nLine 2</p>";
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            break;

          case 'rel':
            $rel = isset($new_value) ? ' rel="' . $new_value . '"' : '';
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url1) . '"' . $rel . '>' . Html::escape($title1) . '</a>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url2) . '"' . $rel . '>' . Html::escape($title2) . '</a>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url3) . '"' . $rel . '>' . Html::escape($title3) . '</a>';
            $expected .= "<p class=\"link-description\">Link 1 &amp; &amp;amp;<br />\nLine 2</p>";
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            break;

          case 'target':
            $target = isset($new_value) ? ' target="' . $new_value . '"' : '';
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url1) . '"' . $target . '>' . Html::escape($title1) . '</a>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url2) . '"' . $target . '>' . Html::escape($title2) . '</a>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            $expected = '<div class="link-item">';
            $expected .= '<a href="' . Html::escape($url3) . '"' . $target . '>' . Html::escape($title3) . '</a>';
            $expected .= "<p class=\"link-description\">Link 1 &amp; &amp;amp;<br />\nLine 2</p>";
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            break;
        }
      }
    }
  }

  /**
   * Tests the 'link_separate_description' formatter.
   *
   * Rework of the test from Drupal\Tests\link\Functional\LinkFieldTest.
   */
  public function testLinkSeparateFormatter() {
    $display_options = [
      'type' => 'link_separate_description',
      'label' => 'hidden',
    ];
    $this->setViewDisplayOptions($display_options);

    // Create an entity with three link field values:
    // The first field item uses a URL only.
    // The second field item uses a URL and link text.
    // The third field item uses a fragment-only URL with text and description.
    $this->drupalGet('entity_test/add');
    $url1 = 'http://www.example.com/content/articles/archive?author=John&year=2012#com';
    $url2 = 'http://www.example.org/content/articles/archive?author=John&year=2012#org';
    $url3 = '#net';
    // Intentionally contains an ampersand that needs sanitization on output.
    $title2 = 'A very long & strange example title that could break the nice layout of the site';
    $title3 = 'Fragment only';
    $edit = [
      "{$this->fieldName}[0][uri]" => $url1,
      "{$this->fieldName}[1][uri]" => $url2,
      "{$this->fieldName}[1][title]" => $title2,
      "{$this->fieldName}[2][uri]" => $url3,
      "{$this->fieldName}[2][title]" => $title3,
      "{$this->fieldName}[2][description]" => "Link 1 & &amp;\nLine 2",
    ];
    $this->submitForm($edit, 'Save');
    preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
    $id = $match[1];
    $this->assertSession()->pageTextContains('entity_test ' . $id . ' has been created.');

    // Verify that the link is output according to the formatter settings.
    $options = [
      'trim_length' => [NULL, 6],
      'rel' => [NULL, 'nofollow'],
      'target' => [NULL, '_blank'],
    ];
    foreach ($options as $setting => $values) {
      foreach ($values as $new_value) {
        // Update the field formatter settings.
        $display_options['settings'] = [$setting => $new_value];
        $this->setViewDisplayOptions($display_options);

        $output = $this->renderTestEntity($id);
        switch ($setting) {
          case 'trim_length':
            $url = $url1;
            $url_title = isset($new_value) ? Unicode::truncate($url, $new_value, FALSE, TRUE) : $url;
            $expected = '<div class="link-item">';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url) . '">' . Html::escape($url_title) . '</a></div>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);

            $url = $url2;
            $url_title = isset($new_value) ? Unicode::truncate($url, $new_value, FALSE, TRUE) : $url;
            $title = isset($new_value) ? Unicode::truncate($title2, $new_value, FALSE, TRUE) : $title2;
            $expected = '<div class="link-item">';
            $expected .= '<div class="link-title">' . Html::escape($title) . '</div>';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url) . '">' . Html::escape($url_title) . '</a></div>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);

            $url = $url3;
            $url_title = isset($new_value) ? Unicode::truncate($url, $new_value, FALSE, TRUE) : $url;
            $title = isset($new_value) ? Unicode::truncate($title3, $new_value, FALSE, TRUE) : $title3;
            $expected = '<div class="link-item">';
            $expected .= '<div class="link-title">' . Html::escape($title) . '</div>';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url) . '">' . Html::escape($url_title) . '</a></div>';
            $expected .= "<p class=\"link-description\">Link 1 &amp; &amp;amp;<br />\nLine 2</p>";
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            break;

          case 'rel':
            $rel = isset($new_value) ? ' rel="' . $new_value . '"' : '';
            $expected = '<div class="link-item">';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url1) . '"' . $rel . '>' . Html::escape($url1) . '</a></div>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);

            $expected = '<div class="link-item">';
            $expected .= '<div class="link-title">' . Html::escape($title2) . '</div>';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url2) . '"' . $rel . '>' . Html::escape($url2) . '</a></div>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);

            $expected = '<div class="link-item">';
            $expected .= '<div class="link-title">' . Html::escape($title3) . '</div>';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url3) . '"' . $rel . '>' . Html::escape($url3) . '</a></div>';
            $expected .= "<p class=\"link-description\">Link 1 &amp; &amp;amp;<br />\nLine 2</p>";
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            break;

          case 'target':
            $target = isset($new_value) ? ' target="' . $new_value . '"' : '';
            $expected = '<div class="link-item">';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url1) . '"' . $target . '>' . Html::escape($url1) . '</a></div>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);

            $expected = '<div class="link-item">';
            $expected .= '<div class="link-title">' . Html::escape($title2) . '</div>';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url2) . '"' . $target . '>' . Html::escape($url2) . '</a></div>';
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);

            $expected = '<div class="link-item">';
            $expected .= '<div class="link-title">' . Html::escape($title3) . '</div>';
            $expected .= '<div class="link-url"><a href="' . Html::escape($url3) . '"' . $target . '>' . Html::escape($url3) . '</a></div>';
            $expected .= "<p class=\"link-description\">Link 1 &amp; &amp;amp;<br />\nLine 2</p>";
            $expected .= '</div>';
            $this->assertStringContainsString($expected, $output);
            break;
        }
      }
    }
  }

  /**
   * Renders a test_entity and returns the output.
   *
   * @param int $id
   *   The test_entity ID to render.
   *
   * @return string
   *   The rendered HTML output.
   */
  protected function renderTestEntity($id) {
    \Drupal::entityTypeManager()->getStorage('entity_test')->resetCache([$id]);
    $entity = EntityTest::load($id);
    $display = \Drupal::service('entity_display.repository')
      ->getViewDisplay($entity->getEntityTypeId(), $entity->bundle(), 'full');
    $content = $display->build($entity);
    $output = \Drupal::service('renderer')->renderRoot($content);
    return (string) $output;
  }

  /**
   * Sets display options for the test field in the test entity.
   *
   * @param array $display_options
   *   Display options.
   */
  protected function setViewDisplayOptions(array $display_options) {
    \Drupal::service('entity_display.repository')
      ->getViewDisplay('entity_test', 'entity_test', 'full')
      ->setComponent($this->fieldName, $display_options)
      ->save();
  }

}
